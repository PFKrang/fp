import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import javafx.scene.image.Image;


public class FPModel {
  private JsonElement jse;

  public boolean getBr(String beer)
  {
    try
    {
    	if ( beer != null && !beer.isEmpty())
    	{
		      URL brURL = new URL("http://api.brewerydb.com/v2/search?q=" + URLEncoder.encode(beer, "UTF-8") + "&type=beer&key=a60d447a6e5c8e71b9fb53a4bb16a529");
		      
		      // Open connection
		      InputStream is = brURL.openStream();
		      BufferedReader br = new BufferedReader(new InputStreamReader(is));
		
		      // Read the results into a JSON Element
		      jse = new JsonParser().parse(br);
		
		      // Close connection
		      is.close();
		      br.close();
    	}

    }
    catch (java.io.UnsupportedEncodingException uee)
    {
      uee.printStackTrace();
    }
    catch (java.net.MalformedURLException mue)
    {
      mue.printStackTrace();
    }
    catch (java.io.IOException ioe)
    {
      ioe.printStackTrace();
    }
    catch (java.lang.NullPointerException npe)
    {
      npe.printStackTrace();
    }

    // Check to see if the beer entered in was valid.
    if (jse != null)
    	return isValid();
    else
    	return false;

  }
	
  
  public boolean isValid()
  {
    if (jse.getAsJsonObject().has("data") 
    		&& jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().has("name")
    		&& jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().has("style")
    		&& jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().has("abv")
    		&& jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().has("description")
    		&& jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().has("labels"))
    		return true;
    	else
    		return false;
  }
  
  public String getName()
  {
	  return jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString();
  }
  
  public String getStyle()
  {
	  return jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("style").getAsJsonObject().get("shortName").getAsString();
  }
  
  public String getStyleD()
  {
	  return jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("style").getAsJsonObject().get("description").getAsString();
  }
  
  public String getAbv()
  {
	  return jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("abv").getAsString();
  }
  
  public String getDesc()
  {
	  return jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("description").getAsString();
  }
  
  public Image getImage()
  {
    String iconURL = jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("labels").getAsJsonObject().get("icon").getAsString();
    return new Image(iconURL);
  }

}
