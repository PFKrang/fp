import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FPModelTest {

	// Create object to access the Model
	FPModel check = new FPModel();
	
	@Test
	public void testGetBr1()
	{
		FPModel b = new FPModel();
		boolean ans = true;
		boolean val = b.getBr("Lagunitas");
		assertEquals(ans, val);
	}
	
	@Test
	public void testGetBr2()
	{
		FPModel b = new FPModel();
		boolean ans = false;
		boolean val = b.getBr("");
		assertEquals(ans, val);
	}
	
	@Test
	public void testGetBName()
	{
		FPModel b = new FPModel();
		b.getBr("Lagunitas");
		String n = b.getName();
		assertEquals("Lagunitas IPA", n);
	}
	
	@Test
	public void testGetStyle()
	{
		FPModel b = new FPModel();
		b.getBr("Lagunitas");
		String n = b.getStyle();
		assertEquals("American IPA", n);
	}
	
	@Test
	public void testGetAbv()
	{
		FPModel b = new FPModel();
		b.getBr("Lagunitas");
		String n = b.getAbv();
		assertEquals("6.2", n);
	}
	
	@Test
	public void testGetStyleD()
	{
		FPModel b = new FPModel();
		b.getBr("Lagunitas");
		String n = b.getStyleD();
		assertEquals("American-style India pale ales are perceived to have medium-high to intense hop bitterness, "
				+ "flavor and aroma with medium-high alcohol content. The style is further characterized by floral, "
				+ "fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one "
				+ "or more of these American-variety hop characters is the perceived end, but the hop characters may "
				+ "be a result of the skillful use of hops of other national origins. The use of water with high mineral "
				+ "content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery "
				+ "hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). "
				+ "India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and "
				+ "aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. "
				+ "Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are "
				+ "considered enough of a distinction justifying separate American-style IPA and English-style IPA categories "
				+ "or subcategories. Hops of other origins may be used for bitterness or approximating traditional American "
				+ "or English character. See English-style India Pale Ale", n);
	}
	
	@Test
	public void testGetDesc()
	{
		FPModel b = new FPModel();
		b.getBr("Lagunitas");
		String n = b.getDesc();
		assertEquals("This is our unique version of an ancient style. A style as old as the ocean trade routes of the last "
				+ "centuries Great Ships. Not as old as the equator they had to cross twice enroute, nor as old as the "
				+ "10,000 or so miles of Di-Hydrogen Oxide and Sodium upon which they sailed, but older than the Circulithium-4 "
				+ "Lentloid that binds the Lupulin Quartnate onto your taste buds. Weird. Think about it. Now stop. OK, go again, "
				+ "now stop. Think again, and stop. But we digress. Made with 43 different hops and 65 various malts, this "
				+ "redolent ale will likely float your boat, whatever planet you're on.", n);
	}
	
}
