import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class FPController implements Initializable {

  @FXML
  private Button btngo;

  @FXML
  private TextField txtbeer;
  
  @FXML
  private Label lblname;

  @FXML
  private TextArea lblstyled;

  @FXML
  private Label lblstyle;
  
  @FXML
  private Label lblabv;
  
  @FXML
  private TextArea lbldesc;
  
  @FXML
  private ImageView icon;

  @FXML
  private void handleButtonAction(ActionEvent e) {
    // Create object to access the Model
    FPModel drink = new FPModel();

    // Has the go button been pressed?
    if (e.getSource() == btngo)
    {
    	lblname.setText("");
    	lbldesc.setText("");
    	lblstyle.setText("");
    	lblstyled.setText("");
    	lblabv.setText("");
    	icon.setImage(new Image("beer.png"));
    	
      String b = txtbeer.getText();
      
      if (drink.getBr(b))
      {
        lblname.setText(drink.getName());
        lblstyle.setText(drink.getStyle());
        lblstyled.setText(drink.getStyleD());
        lblabv.setText(drink.getAbv());
        lbldesc.setText(drink.getDesc());
        icon.setImage(drink.getImage());
      }
      else
      {
        lblname.setText("Invalid Beer Name");
        lblstyle.setText("");
        lblstyled.setText("");
        lblabv.setText("");
        lbldesc.setText("");
        icon.setImage(new Image("error.png"));
      }
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {
	  //TODO
  }    

}
